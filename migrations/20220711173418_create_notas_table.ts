import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('notas', function(table) {
    table.increments();
    
    table.integer('id_aluno').unsigned().notNullable();
    table.foreign('id_aluno').references('id').inTable('alunos');

    table.integer('n1').notNullable();
    table.integer('n2').notNullable();
    table.integer('n3').notNullable();
    table.integer('n4').notNullable();
    
    table.timestamp('created_at').defaultTo(knex.fn.now())
    table.timestamp('updated_at').defaultTo(knex.fn.now())
  })
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('notas');
}

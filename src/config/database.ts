const knex = require('knex');
const config = require("../../knexfile");

export const db = knex(config.development);

import { config as configDotenv } from 'dotenv';
import express from 'express'
import { router } from './routes';

configDotenv();

const app = express();

const port = process.env.PORT || 3000;

app.use(
  express.urlencoded({
    extended: true,
  }),
);
app.use(express.json());

app.use('/', router);

app.listen(port, () => console.log(`Running on port ${port}`));

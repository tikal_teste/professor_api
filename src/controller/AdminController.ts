import { createMethod, getAllStudentsAndGrades } from "../services/AdminService";
import { Validator  } from 'node-input-validator';
import { Request, Response } from "express";

export const create = async (req: Request, res: Response): Promise<any> => {
  const params = req.body;

  const validator = new Validator(params, {
    name: 'required|string',
    n1: 'required|integer',
    n2: 'required|integer',
    n3: 'required|integer',
    n4: 'required|integer'
  });

  validator.check().then((matched) => {
    if (!matched) {
      res.status(422).send(validator.errors);
    }
  });

  try {
    const result = await createMethod(params);

    console.log('success', 'Create method returning success');
    return res.status(200).json({
      message: 200,
      data: result
    })
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
}

export const getAll = async (req: Request, res: Response): Promise<any> => {
  try {
    const result = await getAllStudentsAndGrades();

    console.log('success', 'getAll method returning success');
    return res.status(200).json({
      message: 200,
      data: result
    })
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
}

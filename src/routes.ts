import * as express from 'express';
import { create, getAll } from './controller/AdminController';

export const router = express.Router();

router.post('/create', create);
router.get('/getAll', getAll);

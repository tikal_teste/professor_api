export interface CreateStudentAndGrades {
  name: string;
  n1: number;
  n2: number;
  n3: number;
  n4: number;
}

import { IGrades } from "src/interface/IGrades";
import { CreateStudentAndGrades } from "src/interface/IStudent";
import { db } from "../config/database";

export const createMethod = async (data: CreateStudentAndGrades) => {
  const createStudent = await db('alunos').insert({
    nome: data.name
  }).returning('*');

  if(!createStudent) {
    throw new Error(`Error to create student ${data.name}`);
  }

  return db('notas').insert({
    id_aluno: createStudent[0].id,
    n1: data.n1,
    n2: data.n2,
    n3: data.n3,
    n4: data.n4,
  }).returning('*');
}

export const getAllStudentsAndGrades = async () => {
  const findAllStudent = await db('alunos').select('*');
  const result: any = [];

  const studentsPromise = findAllStudent.map(async (student: any) => {
    const findAllGrades = await db('notas').where({id_aluno: student.id}).returning('*');

    const calculate = calculateStudentGrade(findAllGrades[0]);

    result.push({
      ...student,
      notas: findAllGrades,
      situacao: calculate
    })
  });

  await Promise.all(studentsPromise);

  return result
}

export const calculateStudentGrade = ( data: IGrades ) => {
  const calculateMedia = (data.n1 + data.n2 + data.n3 + data.n4) / 4;
  let status = null;

  if ( calculateMedia >= 6 ) {
    status = "aprovado";
  }

  if ( calculateMedia >= 4 && calculateMedia < 6) {
    status = "recuperacao"
  }

  if ( calculateMedia < 4) {
    status = "reprovado"
  }

  return {
    media: calculateMedia,
    status
  };
}

# Instalação

Instalação das dependências
- npm install

Rodando o Projeto
- npm run dev

Rodando o banco de dados
- knex migrate:latest


# Rotas

- Criação de Aluno e sua Nota
  .post('/create')-> body example = {
    "name": "Aluno 1",
    "n1": 6,
    "n2": 10,
    "n3": 5,
    "n4": 9
  }

- Visualiza a nota de todos os alunos
  .get('/getAll')

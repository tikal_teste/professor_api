# docker build -t {name}-api . // Constrói a imagem conforme instrução
# docker run --name {name}-api -d -p 3001:3001 {name}-api

FROM node:16

WORKDIR /usr/src/app

COPY . .

RUN npm install && npm run build

EXPOSE 3001

CMD [ "node", "./build/index.js" ]
